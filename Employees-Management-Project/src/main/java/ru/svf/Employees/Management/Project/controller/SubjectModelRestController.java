package ru.svf.Employees.Management.Project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.svf.Employees.Management.Project.entity.SubjectModel;
import ru.svf.Employees.Management.Project.model.request.AddProfessorInGroupRequest;
import ru.svf.Employees.Management.Project.model.request.AddStudentsInSubjectRequest;
import ru.svf.Employees.Management.Project.model.request.SubjectModelRequest;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;
import ru.svf.Employees.Management.Project.model.response.SubjectModelResponse;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;
import ru.svf.Employees.Management.Project.service.interf.ISubjectModelService;

import java.util.List;

@RestController
@RequestMapping(SubjectModelRestController.SUBJECT_MODEL_REST_CONTROLLER)
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:5173")
public class SubjectModelRestController extends CRUDRESTController<SubjectModelRequest, SubjectModelResponse, String, SubjectModel> {
    public static final String SUBJECT_MODEL_REST_CONTROLLER = "/subjects";
    private final ISubjectModelService subjectModelService;

    @Override
    CRUDService<SubjectModelRequest, SubjectModelResponse, String, SubjectModel> getService() {
        return subjectModelService;
    }

    @PostMapping("/add-students")
    public ResponseEntity<List<StudentModelResponse>> addStudentsInSubject(
            @RequestBody AddStudentsInSubjectRequest addStudentsInSubjectRequest
    ) {
        return new ResponseEntity<>(subjectModelService.addStudentsInSubjects(addStudentsInSubjectRequest), HttpStatus.OK);
    }
}
