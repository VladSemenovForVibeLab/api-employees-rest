package ru.svf.Employees.Management.Project.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubjectModelRequest {
    private String sId;
    private String sName;
}
