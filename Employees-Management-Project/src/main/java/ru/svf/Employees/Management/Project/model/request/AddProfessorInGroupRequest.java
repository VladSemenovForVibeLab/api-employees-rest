package ru.svf.Employees.Management.Project.model.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddProfessorInGroupRequest {
    List<String> professorIds;
    List<String> groupIds;
}
