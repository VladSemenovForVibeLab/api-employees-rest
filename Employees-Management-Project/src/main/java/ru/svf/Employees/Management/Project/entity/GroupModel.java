package ru.svf.Employees.Management.Project.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = GroupModel.TABLE_NAME)
public class GroupModel {
    public static final String TABLE_NAME = "groups";
    private static final String GID = "g_id";
    private static final String GNAME = "g_name";
    private static final String GNUMBEROFSTUDENTS="g_number_of_students";
    @Id
    @org.springframework.data.annotation.Id
    @Column(name = GID)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String gId;
    @Column(name = GNAME,   nullable = false)
    private String gName;
    @Column(name = GNUMBEROFSTUDENTS,  nullable = false)
    private int gNumberOfStudents;
    @ManyToMany
    @JoinTable(
            name = "group_professors",
    joinColumns = @JoinColumn(name = "group_id"),
    inverseJoinColumns = @JoinColumn(name = "professor_id")
    )
    private Set<ProfessorModel> professors;
    @OneToMany(mappedBy = "group")
    private Set<StudentModel> students;

}
