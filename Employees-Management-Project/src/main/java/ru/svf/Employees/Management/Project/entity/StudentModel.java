package ru.svf.Employees.Management.Project.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = StudentModel.TABLE_NAME)
public class StudentModel {
    public static final String TABLE_NAME = "students";
    private static final String SID = "s_id";
    private static final String SNAME = "s_name";
    private static final String SEMAIL = "s_email";
    private static final String SPHONE = "s_phone";
    @Id
    @org.springframework.data.annotation.Id
    @Column(name = SID)
    @Setter(AccessLevel.NONE)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String sId;
    @Column(name = SNAME,   nullable = false)
    private String sName;
    @Column(name = SEMAIL,  nullable = false)
    private String sEmail;
    @Column(name = SPHONE, nullable = false)
    private String sPhone;
    @ManyToOne
    @JoinColumn(name = "professor_id")
    private ProfessorModel professor;
    @ManyToOne
    @JoinColumn(name = "course_id")
    private CourseModel course;
    @ManyToOne
    @JoinColumn(name = "group_id")
    private GroupModel group;
    @ManyToMany
    @JoinTable(
            name = "student_subject",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id")
    )
    private Set<SubjectModel> subjects;
    public StudentModel(String sName, String sPhone, String sEmail) {
        this.sName = sName;
        this.sPhone = sPhone;
        this.sEmail = sEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentModel that = (StudentModel) o;
        return Objects.equals(sId, that.sId) && Objects.equals(sName, that.sName) && Objects.equals(sEmail, that.sEmail) && Objects.equals(sPhone, that.sPhone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sId, sName, sEmail, sPhone);
    }
}
