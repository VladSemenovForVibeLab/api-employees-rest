package ru.svf.Employees.Management.Project.util;

import org.springframework.stereotype.Component;
import ru.svf.Employees.Management.Project.entity.CourseModel;
import ru.svf.Employees.Management.Project.model.request.CourseModelRequest;
import ru.svf.Employees.Management.Project.model.response.CourseModelResponse;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CourseModelDTOUtil implements EntityDTOUtil<CourseModel, CourseModelRequest, CourseModelResponse> {
    @Override
    public CourseModelRequest toDtoModelRequest(CourseModel entityModel) {
        return CourseModelRequest.builder()
                .cId(entityModel.getCId()).cName(entityModel.getCName()).cDuration(entityModel.getCDuration()).build();
    }

    @Override
    public CourseModelResponse toDtoModelResponseFromDtoModelRequest(CourseModelRequest dtoModel) {
        return CourseModelResponse.builder().cId(dtoModel.getCId()).cName(dtoModel.getCName()).cDuration(dtoModel.getCDuration()).build();
    }

    @Override
    public CourseModelResponse toDtoModelResponse(CourseModel entityModel) {
        return CourseModelResponse.builder().cId(entityModel.getCId()).cName(entityModel.getCName()).cDuration(entityModel.getCDuration()).build();
    }

    @Override
    public CourseModel toEntityModelFromRequest(CourseModelRequest dtoModel) {
        return CourseModel.builder().cId(dtoModel.getCId()).cName(dtoModel.getCName()).cDuration(dtoModel.getCDuration()).build();
    }

    @Override
    public CourseModel toEntityModelFromResposne(CourseModelResponse dtoModel) {
        return CourseModel
                .builder()
                .cId(dtoModel.getCId())
                .cName(dtoModel.getCName())
                .cDuration(dtoModel.getCDuration())
                .build();
    }

    @Override
    public List<CourseModelRequest> toDtoModelsRequests(List<CourseModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseModelResponse> toDtoModelsResponse(List<CourseModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseModel> toEntityModelsFromRequest(List<CourseModelRequest> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseModel> toEntityModelsFromResponse(List<CourseModelResponse> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromResposne)
                .collect(Collectors.toList());
    }
}
