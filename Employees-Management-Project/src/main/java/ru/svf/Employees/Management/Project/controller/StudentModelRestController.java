package ru.svf.Employees.Management.Project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.svf.Employees.Management.Project.entity.StudentModel;
import ru.svf.Employees.Management.Project.model.request.StudentModelRequest;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;
import ru.svf.Employees.Management.Project.service.interf.StudentModelService;

@RestController
@RequestMapping(StudentModelRestController.STUDENT_MODEL_REST_CONTROLLER)
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:5173")
public class StudentModelRestController extends CRUDRESTController<StudentModelRequest, StudentModelResponse, String, StudentModel> {
    private final StudentModelService studentModelService;
    public static final String STUDENT_MODEL_REST_CONTROLLER = "/students";

    @Override
    CRUDService<StudentModelRequest, StudentModelResponse, String, StudentModel> getService() {
        return studentModelService;
    }

    @PatchMapping("/addProfessorForStudent/{sid}/{pid}")
    public ResponseEntity<Void> addProfessorForStudent(
            @PathVariable String sid,
            @PathVariable String pid
    ) {
        studentModelService.addProfessorForStudent(sid, pid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/addCourseForStudent/{sid}/{cid}")
    public ResponseEntity<Void> addCourseForStudent(
            @PathVariable String sid,
            @PathVariable String cid
    ) {
        studentModelService.addCourseForStudent(sid, cid);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/addGroupForStudent/{sid}/{gid}")
    public ResponseEntity<Void> addGroupForStudent(
            @PathVariable String sid,
            @PathVariable String gid
    ) {
        studentModelService.addGroupForStudent(sid,gid);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
