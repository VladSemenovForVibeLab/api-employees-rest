package ru.svf.Employees.Management.Project.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GroupModelRequest {
    private String gId;
    private String gName;
    private int gNumberOfStudents;
}
