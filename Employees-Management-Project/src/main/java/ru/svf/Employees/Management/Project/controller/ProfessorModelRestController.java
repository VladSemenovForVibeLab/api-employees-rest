package ru.svf.Employees.Management.Project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.model.request.ProfessorModelRequest;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;
import ru.svf.Employees.Management.Project.service.interf.ProfessorModelService;

@RestController
@RequestMapping(ProfessorModelRestController.PROFESSOR_MODEL_REST_CONTROLLER)
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:5173")
public class ProfessorModelRestController  extends CRUDRESTController<ProfessorModelRequest, ProfessorModelResponse,String, ProfessorModel>{
    private final ProfessorModelService professorModelService;
    public static final String PROFESSOR_MODEL_REST_CONTROLLER = "/professors";

    @Override
    CRUDService<ProfessorModelRequest, ProfessorModelResponse, String, ProfessorModel> getService() {
        return professorModelService;
    }
}
