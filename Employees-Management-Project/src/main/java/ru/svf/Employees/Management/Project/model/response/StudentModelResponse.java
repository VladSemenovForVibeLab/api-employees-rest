package ru.svf.Employees.Management.Project.model.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentModelResponse {
    private String sId;
    private String sName;
    private String sEmail;
    private String sPhone;
}
