package ru.svf.Employees.Management.Project.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.svf.Employees.Management.Project.entity.CourseModel;
import ru.svf.Employees.Management.Project.entity.GroupModel;
import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.entity.StudentModel;
import ru.svf.Employees.Management.Project.model.request.StudentModelRequest;
import ru.svf.Employees.Management.Project.model.response.CourseModelResponse;
import ru.svf.Employees.Management.Project.model.response.GroupModelResponse;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;
import ru.svf.Employees.Management.Project.repository.StudentModelRepository;
import ru.svf.Employees.Management.Project.service.interf.CourseModelService;
import ru.svf.Employees.Management.Project.service.interf.IGroupModelService;
import ru.svf.Employees.Management.Project.service.interf.ProfessorModelService;
import ru.svf.Employees.Management.Project.service.interf.StudentModelService;
import ru.svf.Employees.Management.Project.util.CourseModelDTOUtil;
import ru.svf.Employees.Management.Project.util.EntityDTOUtil;
import ru.svf.Employees.Management.Project.util.GroupModelDTOUtil;
import ru.svf.Employees.Management.Project.util.ProfessorModelDTOUtil;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class StudentModelServiceImpl extends CRUDServiceImpl<StudentModelRequest, StudentModelResponse, String, StudentModel> implements StudentModelService{
    private final StudentModelRepository studentModelRepository;
    private final ProfessorModelService professorModelService;
    private final ProfessorModelDTOUtil professorModelDTOUtil;
    private final CourseModelService courseModelService;
    private final CourseModelDTOUtil courseModelDTOUtil;
    private final IGroupModelService groupModelService;
    private final GroupModelDTOUtil groupModelDTOUtil;
    private final EntityDTOUtil<StudentModel, StudentModelRequest, StudentModelResponse> studentModelEntityDTO;
    @Override
    JpaRepository<StudentModel, String> getJpaRepository() {
        return studentModelRepository;
    }

    @Override
    EntityDTOUtil<StudentModel, StudentModelRequest, StudentModelResponse> getEntityDTOUtil() {
        return studentModelEntityDTO;
    }

    @Override
    @Transactional
    public void addProfessorForStudent(String sId, String pId) {
        StudentModel studentModel = findStudentById(sId);
        ProfessorModelResponse professorModel = professorModelService.readModelById(pId);
        studentModel.setProfessor(professorModelDTOUtil.toEntityModelFromResposne(professorModel));
    }
    @Override
    @Transactional
    public void addCourseForStudent(String sId, String cId) {
        StudentModel studentModel = findStudentById(sId);
        CourseModelResponse courseModelResponse = courseModelService.readModelById(cId);
        CourseModel entityModelFromResponse = courseModelDTOUtil.toEntityModelFromResposne(courseModelResponse);
        studentModel.setCourse(entityModelFromResponse);
        studentModelRepository.save(studentModel);
    }

    @Override
    @Transactional
    public void addGroupForStudent(String sId, String gId) {
        StudentModel studentModel = findStudentById(sId);
        GroupModelResponse groupModelResponse = groupModelService.readModelById(gId);
        GroupModel entityModelFromResponse = groupModelDTOUtil.toEntityModelFromResposne(groupModelResponse);
        studentModel.setGroup(entityModelFromResponse);
        studentModelRepository.save(studentModel);
    }


    private StudentModel findStudentById(String sId){
        return studentModelRepository.findById(sId)
              .orElseThrow(()->new IllegalStateException("Студент не найден"));
    }
}
