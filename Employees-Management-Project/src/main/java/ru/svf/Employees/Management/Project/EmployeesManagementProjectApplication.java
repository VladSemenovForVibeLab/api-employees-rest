package ru.svf.Employees.Management.Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeesManagementProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeesManagementProjectApplication.class, args);
	}

}
