package ru.svf.Employees.Management.Project.util;

import org.springframework.stereotype.Component;
import ru.svf.Employees.Management.Project.entity.SubjectModel;
import ru.svf.Employees.Management.Project.model.request.SubjectModelRequest;
import ru.svf.Employees.Management.Project.model.response.SubjectModelResponse;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SubjectModelDTOUtil implements EntityDTOUtil<SubjectModel, SubjectModelRequest, SubjectModelResponse>{
    @Override
    public SubjectModelRequest toDtoModelRequest(SubjectModel entityModel) {
        return SubjectModelRequest
                .builder()
                .sId(entityModel.getSId())
                .sName(entityModel.getSName())
                .build();
    }

    @Override
    public SubjectModelResponse toDtoModelResponseFromDtoModelRequest(SubjectModelRequest dtoModel) {
        return SubjectModelResponse
                .builder()
                .sId(dtoModel.getSId())
                .sName(dtoModel.getSName())
                .build();
    }

    @Override
    public SubjectModelResponse toDtoModelResponse(SubjectModel entityModel) {
        return SubjectModelResponse
                .builder()
                .sId(entityModel.getSId())
                .sName(entityModel.getSName())
                .build();
    }

    @Override
    public SubjectModel toEntityModelFromRequest(SubjectModelRequest dtoModel) {
        return SubjectModel
                .builder()
                .sId(dtoModel.getSId())
                .sName(dtoModel.getSName())
                .build();
    }

    @Override
    public SubjectModel toEntityModelFromResposne(SubjectModelResponse dtoModel) {
        return SubjectModel
                .builder()
                .sId(dtoModel.getSId())
                .sName(dtoModel.getSName())
                .build();
    }

    @Override
    public List<SubjectModelRequest> toDtoModelsRequests(List<SubjectModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<SubjectModelResponse> toDtoModelsResponse(List<SubjectModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<SubjectModel> toEntityModelsFromRequest(List<SubjectModelRequest> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<SubjectModel> toEntityModelsFromResponse(List<SubjectModelResponse> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromResposne)
                .collect(Collectors.toList());
    }
}
