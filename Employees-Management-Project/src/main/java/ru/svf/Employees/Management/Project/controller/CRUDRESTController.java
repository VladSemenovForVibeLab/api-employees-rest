package ru.svf.Employees.Management.Project.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;

import java.util.List;

public abstract class CRUDRESTController<R,P,S,E> {
    abstract CRUDService<R,P,S,E> getService();

    @PostMapping("/createEntity")
    public ResponseEntity<P> createEntity(@RequestBody R entity) {
        return new ResponseEntity<>(getService().createModel(entity), HttpStatus.CREATED);
    }
    @GetMapping("/findEntityResponseById/{id}")
    public ResponseEntity<P> findEntityById(@PathVariable("id") S id) {
        return new ResponseEntity<>(getService().readModelById(id), HttpStatus.OK);
    }
    @GetMapping("/findAllEntities")
    public ResponseEntity<List<P>> findAllEntities(){
        return new ResponseEntity<>(getService().readModels(), HttpStatus.OK);
    }
    @PutMapping("/updateEntity")
    public ResponseEntity<P> updateEntity(@RequestBody R entity) {
        return new ResponseEntity<>(getService().updateModel(entity), HttpStatus.OK);
    }
    @DeleteMapping("/deleteEntity/{id}")
    public ResponseEntity<Boolean> deleteEntity(@PathVariable S id){
        return new ResponseEntity<>(getService().delete(id), HttpStatus.OK);
    }
}
