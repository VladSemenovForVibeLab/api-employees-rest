package ru.svf.Employees.Management.Project.service.interf;

import ru.svf.Employees.Management.Project.entity.StudentModel;
import ru.svf.Employees.Management.Project.model.request.StudentModelRequest;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;

public interface StudentModelService extends CRUDService<StudentModelRequest, StudentModelResponse,String, StudentModel> {
    void addProfessorForStudent(String sId,String pId);
    void addCourseForStudent(String sId,String cId);

    void addGroupForStudent(String sId,String gId);
}
