package ru.svf.Employees.Management.Project.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.svf.Employees.Management.Project.entity.GroupModel;
import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.model.request.AddProfessorInGroupRequest;
import ru.svf.Employees.Management.Project.model.request.GroupModelRequest;
import ru.svf.Employees.Management.Project.model.response.GroupModelResponse;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;
import ru.svf.Employees.Management.Project.repository.GroupModelRepository;
import ru.svf.Employees.Management.Project.repository.ProfessorModelRepository;
import ru.svf.Employees.Management.Project.service.interf.IGroupModelService;
import ru.svf.Employees.Management.Project.util.EntityDTOUtil;
import ru.svf.Employees.Management.Project.util.GroupModelDTOUtil;
import ru.svf.Employees.Management.Project.util.ProfessorModelDTOUtil;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupModelServiceImpl extends CRUDServiceImpl<GroupModelRequest, GroupModelResponse, String, GroupModel> implements IGroupModelService {
    private final GroupModelRepository groupModelRepository;
    private final GroupModelDTOUtil groupModelDTOUtil;
    private final ProfessorModelDTOUtil professorModelDTOUtil;
    private final ProfessorModelRepository professorModelRepository;

    @Override
    JpaRepository<GroupModel, String> getJpaRepository() {
        return groupModelRepository;
    }

    @Override
    EntityDTOUtil<GroupModel, GroupModelRequest, GroupModelResponse> getEntityDTOUtil() {
        return groupModelDTOUtil;
    }

    @Override
    @Transactional
    public List<ProfessorModelResponse> addProfessorInGroup(AddProfessorInGroupRequest addProfessorInGroupRequest) {
        List<GroupModel> groups = groupModelRepository.findAllById(addProfessorInGroupRequest.getGroupIds());
        groups.forEach(group -> {
            List<ProfessorModel> professorToAdd = professorModelRepository.findAllById(addProfessorInGroupRequest.getProfessorIds());
            group.getProfessors().addAll(professorToAdd);
        });
        groupModelRepository.saveAll(groups);
        List<ProfessorModel> professorModels = professorModelRepository.findAllById(addProfessorInGroupRequest.getProfessorIds());
        professorModels.forEach(professorModel -> {
            List<GroupModel> groupModelsAdd = groupModelRepository.findAllById(addProfessorInGroupRequest.getGroupIds());
            professorModel.getGroupModels().addAll(groupModelsAdd);
        });
        return professorModels
                .stream()
                .map(professorModelDTOUtil::toDtoModelResponse)
                .collect(Collectors.toList());
    }
}
