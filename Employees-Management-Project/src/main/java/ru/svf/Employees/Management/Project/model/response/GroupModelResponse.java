package ru.svf.Employees.Management.Project.model.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GroupModelResponse {
    private String gId;
    private String gName;
    private int gNumberOfStudents;
}
