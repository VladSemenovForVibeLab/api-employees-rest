package ru.svf.Employees.Management.Project.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.svf.Employees.Management.Project.entity.GroupModel;
import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.entity.StudentModel;
import ru.svf.Employees.Management.Project.entity.SubjectModel;
import ru.svf.Employees.Management.Project.model.request.AddStudentsInSubjectRequest;
import ru.svf.Employees.Management.Project.model.request.SubjectModelRequest;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;
import ru.svf.Employees.Management.Project.model.response.SubjectModelResponse;
import ru.svf.Employees.Management.Project.repository.StudentModelRepository;
import ru.svf.Employees.Management.Project.repository.SubjectModelRepository;
import ru.svf.Employees.Management.Project.service.interf.ISubjectModelService;
import ru.svf.Employees.Management.Project.util.EntityDTOUtil;
import ru.svf.Employees.Management.Project.util.StudentModelDTOUtil;
import ru.svf.Employees.Management.Project.util.SubjectModelDTOUtil;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SubjectModelServiceImpl extends CRUDServiceImpl<SubjectModelRequest, SubjectModelResponse, String, SubjectModel> implements ISubjectModelService {
    private final SubjectModelRepository subjectModelRepository;
    private final SubjectModelDTOUtil subjectModelDTOUtil;
    private final StudentModelRepository studentModelRepository;
    private final StudentModelDTOUtil studentModelDTOUtil;
    @Override
    JpaRepository<SubjectModel, String> getJpaRepository() {
        return subjectModelRepository;
    }

    @Override
    EntityDTOUtil<SubjectModel, SubjectModelRequest, SubjectModelResponse> getEntityDTOUtil() {
        return subjectModelDTOUtil;
    }

    @Override
    @Transactional
    public List<StudentModelResponse> addStudentsInSubjects(AddStudentsInSubjectRequest addStudentsInSubjectRequest) {
        List<StudentModel> studentModels = studentModelRepository.findAllById(addStudentsInSubjectRequest.getStudentIds());
        studentModels.forEach(studentModel -> {
            List<SubjectModel> subjectModelsToAdd = subjectModelRepository.findAllById(addStudentsInSubjectRequest.getSubjectIds());
            studentModel.getSubjects().addAll(subjectModelsToAdd);
        });
        studentModelRepository.saveAll(studentModels);
        List<SubjectModel> subjectModels = subjectModelRepository.findAllById(addStudentsInSubjectRequest.getSubjectIds());
        subjectModels.forEach(subjectModel -> {
            List<StudentModel> studentModelListToAdd = studentModelRepository.findAllById(addStudentsInSubjectRequest.getStudentIds());
            subjectModel.getStudents().addAll(studentModelListToAdd);
        });
        return studentModels
                .stream()
                .map(studentModelDTOUtil::toDtoModelResponse)
                .collect(Collectors.toList());
    }
}
