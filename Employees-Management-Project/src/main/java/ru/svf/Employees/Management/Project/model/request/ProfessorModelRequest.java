package ru.svf.Employees.Management.Project.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProfessorModelRequest {
    private String sId;
    private String sName;
    private String sEmail;
    private String sPhone;
}
