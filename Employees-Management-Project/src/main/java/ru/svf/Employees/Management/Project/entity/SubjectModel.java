package ru.svf.Employees.Management.Project.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = SubjectModel.TABLE_NAME)
public class SubjectModel {
    public static final String TABLE_NAME = "subjects";
    private static final String SID = "s_id";
    private static final String SNAME = "s_name";
    @Id
    @org.springframework.data.annotation.Id
    @Column(name = SID)
    @Setter(AccessLevel.NONE)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String sId;
    @Column(name = SNAME,   nullable = false)
    private String sName;
    @ManyToMany(mappedBy = "subjects")
    private Set<StudentModel> students;
}
