package ru.svf.Employees.Management.Project.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.svf.Employees.Management.Project.entity.CourseModel;
import ru.svf.Employees.Management.Project.model.request.CourseModelRequest;
import ru.svf.Employees.Management.Project.model.response.CourseModelResponse;
import ru.svf.Employees.Management.Project.repository.CourseModelRepository;
import ru.svf.Employees.Management.Project.service.interf.CourseModelService;
import ru.svf.Employees.Management.Project.util.CourseModelDTOUtil;
import ru.svf.Employees.Management.Project.util.EntityDTOUtil;

@Service
@RequiredArgsConstructor
public class CourseModelServiceImpl extends CRUDServiceImpl<CourseModelRequest, CourseModelResponse, String, CourseModel> implements CourseModelService {
    private final CourseModelRepository courseModelRepository;
    private final CourseModelDTOUtil courseModelDTOUtil;
    @Override
    JpaRepository<CourseModel, String> getJpaRepository() {
        return courseModelRepository;
    }

    @Override
    EntityDTOUtil<CourseModel, CourseModelRequest, CourseModelResponse> getEntityDTOUtil() {
        return courseModelDTOUtil;
    }
}
