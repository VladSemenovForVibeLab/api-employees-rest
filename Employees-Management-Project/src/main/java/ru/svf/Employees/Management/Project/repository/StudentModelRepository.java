package ru.svf.Employees.Management.Project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.svf.Employees.Management.Project.entity.StudentModel;

@RepositoryRestResource
@Repository
public interface StudentModelRepository extends JpaRepository<StudentModel, String> {

}