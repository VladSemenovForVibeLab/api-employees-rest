package ru.svf.Employees.Management.Project.service.interf;

import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.model.request.ProfessorModelRequest;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;

public interface ProfessorModelService extends CRUDService<ProfessorModelRequest, ProfessorModelResponse,String, ProfessorModel> {
}
