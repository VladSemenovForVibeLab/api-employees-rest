package ru.svf.Employees.Management.Project.service.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;
import ru.svf.Employees.Management.Project.util.EntityDTOUtil;

import java.util.List;

public abstract class CRUDServiceImpl<R,P,S,E> implements CRUDService<R,P,S,E> {
    abstract JpaRepository<E,S> getJpaRepository();
    abstract EntityDTOUtil<E,R,P> getEntityDTOUtil();
    @Override
    @Transactional
    public P createModel(R entityRequest) {
        return getEntityDTOUtil()
                .toDtoModelResponse(
                        getJpaRepository()
                                .save(
                                        getEntityDTOUtil().toEntityModelFromRequest(entityRequest))
                );
    }

    @Override
    @Transactional
    public P updateModel(R entityRequest) {
        return getEntityDTOUtil()
                .toDtoModelResponse(
                        getJpaRepository()
                               .save(
                                        getEntityDTOUtil().toEntityModelFromRequest(entityRequest))
                );
    }

    @Override
    @Transactional(readOnly = true)
    public List<P> readModels() {
        return getEntityDTOUtil()
                .toDtoModelsResponse(
                        getJpaRepository()
                              .findAll()
                );
    }

    @Override
    @Transactional
    public boolean delete(S id) {
        try {
            getJpaRepository().deleteById(id);
            return true;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public P readModelById(S id) {
        return getEntityDTOUtil()
                .toDtoModelResponse(
                        getJpaRepository()
                                .findById(id)
                                .orElse(null)
                );
    }
}
