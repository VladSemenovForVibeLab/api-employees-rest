package ru.svf.Employees.Management.Project.model.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubjectModelResponse {
    private String sId;
    private String sName;
}
