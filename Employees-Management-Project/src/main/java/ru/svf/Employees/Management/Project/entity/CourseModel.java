package ru.svf.Employees.Management.Project.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = CourseModel.TABLE_NAME)
public class CourseModel {
    public static final String TABLE_NAME = "courses";
    private static final String CID = "c_id";
    private static final String CNAME = "c_name";
    private static final String CDURATION = "c_duration";
    @Id
    @org.springframework.data.annotation.Id
    @Column(name = CID)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    private String cId;
    @Column(name = CNAME,   nullable = false)
    private String cName;
    @Column(name = CDURATION,  nullable = false)
    private String cDuration;
    @OneToMany(mappedBy = "course")
    private Set<StudentModel> students;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseModel that = (CourseModel) o;
        return Objects.equals(cId, that.cId) && Objects.equals(cName, that.cName) && Objects.equals(cDuration, that.cDuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cId, cName, cDuration);
    }
}
