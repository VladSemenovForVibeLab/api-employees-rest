package ru.svf.Employees.Management.Project.model.response;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProfessorModelResponse {
    private String sId;
    private String sName;
    private String sEmail;
    private String sPhone;
}
