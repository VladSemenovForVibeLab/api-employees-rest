package ru.svf.Employees.Management.Project.service.interf;

import java.util.List;

public interface CRUDService<R,P,S,E> {
    P createModel(R entityRequest);
    P updateModel(R entityRequest);
    List<P> readModels();
    boolean delete (S id);
    P readModelById(S id);
}
