package ru.svf.Employees.Management.Project.util;

import org.springframework.stereotype.Component;
import ru.svf.Employees.Management.Project.entity.GroupModel;
import ru.svf.Employees.Management.Project.model.request.GroupModelRequest;
import ru.svf.Employees.Management.Project.model.response.GroupModelResponse;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GroupModelDTOUtil implements EntityDTOUtil<GroupModel, GroupModelRequest, GroupModelResponse>{
    @Override
    public GroupModelRequest toDtoModelRequest(GroupModel entityModel) {
        return GroupModelRequest
                .builder()
                .gId(entityModel.getGId())
                .gName(entityModel.getGName())
                .gNumberOfStudents(entityModel.getGNumberOfStudents())
                .build();
    }

    @Override
    public GroupModelResponse toDtoModelResponseFromDtoModelRequest(GroupModelRequest dtoModel) {
        return GroupModelResponse
                .builder()
                .gId(dtoModel.getGId())
                .gNumberOfStudents(dtoModel.getGNumberOfStudents())
                .gName(dtoModel.getGName())
                .build();
    }

    @Override
    public GroupModelResponse toDtoModelResponse(GroupModel entityModel) {
        return GroupModelResponse
                .builder()
                .gId(entityModel.getGId())
                .gName(entityModel.getGName())
                .gNumberOfStudents(entityModel.getGNumberOfStudents())
                .build();
    }

    @Override
    public GroupModel toEntityModelFromRequest(GroupModelRequest dtoModel) {
        return GroupModel
                .builder()
                .gId(dtoModel.getGId())
                .gName(dtoModel.getGName())
                .gNumberOfStudents(dtoModel.getGNumberOfStudents())
                .build();
    }

    @Override
    public GroupModel toEntityModelFromResposne(GroupModelResponse dtoModel) {
            return GroupModel
                    .builder()
                    .gId(dtoModel.getGId())
                    .gNumberOfStudents(dtoModel.getGNumberOfStudents())
                    .gName(dtoModel.getGName())
                    .build();
    }

    @Override
    public List<GroupModelRequest> toDtoModelsRequests(List<GroupModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<GroupModelResponse> toDtoModelsResponse(List<GroupModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<GroupModel> toEntityModelsFromRequest(List<GroupModelRequest> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<GroupModel> toEntityModelsFromResponse(List<GroupModelResponse> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromResposne)
                .collect(Collectors.toList());
    }
}
