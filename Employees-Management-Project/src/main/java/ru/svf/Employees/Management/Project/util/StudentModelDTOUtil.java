package ru.svf.Employees.Management.Project.util;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.svf.Employees.Management.Project.entity.StudentModel;
import ru.svf.Employees.Management.Project.model.request.StudentModelRequest;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StudentModelDTOUtil implements EntityDTOUtil<StudentModel, StudentModelRequest, StudentModelResponse> {
    @Override
    public StudentModelRequest toDtoModelRequest(StudentModel entityModel) {
        return StudentModelRequest
                .builder()
                .sEmail(entityModel.getSEmail())
                .sId(entityModel.getSId())
                .sName(entityModel.getSName())
                .sPhone(entityModel.getSPhone())
                .build();
    }

    @Override
    public StudentModelResponse toDtoModelResponseFromDtoModelRequest(StudentModelRequest dtoModel) {
        return null;
    }

    @Override
    public StudentModelResponse toDtoModelResponse(StudentModel entityModel) {
        return StudentModelResponse
                .builder()
                .sId(entityModel.getSId())
                .sEmail(entityModel.getSEmail())
                .sName(entityModel.getSName())
                .sPhone(entityModel.getSPhone())
                .build();
    }

    @Override
    public StudentModel toEntityModelFromRequest(StudentModelRequest dtoModel) {
        return StudentModel
                .builder()
                .sEmail(dtoModel.getSEmail())
                .sPhone(dtoModel.getSPhone())
                .sName(dtoModel.getSName())
                .sId(dtoModel.getSId())
                .build();
    }

    @Override
    public StudentModel toEntityModelFromResposne(StudentModelResponse dtoModel) {
        return StudentModel
                .builder()
                .sId(dtoModel.getSId())
                .sEmail(dtoModel.getSEmail())
                .sPhone(dtoModel.getSPhone())
                .sName(dtoModel.getSName())
                .build();
    }

    @Override
    public List<StudentModelRequest> toDtoModelsRequests(List<StudentModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentModelResponse> toDtoModelsResponse(List<StudentModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentModel> toEntityModelsFromRequest(List<StudentModelRequest> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<StudentModel> toEntityModelsFromResponse(List<StudentModelResponse> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromResposne)
                .collect(Collectors.toList());
    }
}
