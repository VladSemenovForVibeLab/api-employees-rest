package ru.svf.Employees.Management.Project.service.interf;

import ru.svf.Employees.Management.Project.entity.CourseModel;
import ru.svf.Employees.Management.Project.model.request.CourseModelRequest;
import ru.svf.Employees.Management.Project.model.response.CourseModelResponse;

public interface CourseModelService extends CRUDService<CourseModelRequest, CourseModelResponse,String, CourseModel>{

}
