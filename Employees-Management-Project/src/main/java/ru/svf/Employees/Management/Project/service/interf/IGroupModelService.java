package ru.svf.Employees.Management.Project.service.interf;

import ru.svf.Employees.Management.Project.entity.GroupModel;
import ru.svf.Employees.Management.Project.model.request.AddProfessorInGroupRequest;
import ru.svf.Employees.Management.Project.model.request.GroupModelRequest;
import ru.svf.Employees.Management.Project.model.response.GroupModelResponse;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;

import java.util.List;

public interface IGroupModelService extends CRUDService<GroupModelRequest, GroupModelResponse,String, GroupModel> {
    List<ProfessorModelResponse> addProfessorInGroup(AddProfessorInGroupRequest addProfessorInGroupRequest);
}
