package ru.svf.Employees.Management.Project.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = ProfessorModel.TABLE_NAME)
public class ProfessorModel {
    public static final String TABLE_NAME = "professors";
    private static final String PID = "p_id";
    private static final String PNAME = "p_name";
    private static final String PEMAIL = "p_email";
    private static final String PPHONE = "p_phone";
    @Id
    @org.springframework.data.annotation.Id
    @Column(name = PID)
    @Setter(AccessLevel.NONE)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String sId;
    @Column(name = PNAME,   nullable = false)
    private String sName;
    @Column(name = PEMAIL,  nullable = false)
    private String sEmail;
    @Column(name = PPHONE, nullable = false)
    private String sPhone;
    @OneToMany(mappedBy = "professor")
    private Set<StudentModel> students;
    @ManyToMany(mappedBy = "professors")
    private Set<GroupModel> groupModels;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfessorModel that = (ProfessorModel) o;
        return Objects.equals(sId, that.sId) && Objects.equals(sName, that.sName) && Objects.equals(sEmail, that.sEmail) && Objects.equals(sPhone, that.sPhone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sId, sName, sEmail, sPhone);
    }
}
