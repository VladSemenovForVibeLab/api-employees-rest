package ru.svf.Employees.Management.Project.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.model.request.ProfessorModelRequest;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;
import ru.svf.Employees.Management.Project.repository.ProfessorModelRepository;
import ru.svf.Employees.Management.Project.service.interf.ProfessorModelService;
import ru.svf.Employees.Management.Project.util.EntityDTOUtil;

@Service
@RequiredArgsConstructor
public class ProfessorModelServiceImpl extends CRUDServiceImpl<ProfessorModelRequest, ProfessorModelResponse, String, ProfessorModel> implements ProfessorModelService {
    private final ProfessorModelRepository professorModelRepository;
    private final EntityDTOUtil<ProfessorModel, ProfessorModelRequest, ProfessorModelResponse> professorModelEntityDTO;
    @Override
    JpaRepository<ProfessorModel, String> getJpaRepository() {
        return professorModelRepository;
    }

    @Override
    EntityDTOUtil<ProfessorModel, ProfessorModelRequest, ProfessorModelResponse> getEntityDTOUtil() {
        return professorModelEntityDTO;
    }
}
