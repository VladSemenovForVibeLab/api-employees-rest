package ru.svf.Employees.Management.Project.model.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CourseModelResponse {
    private String cId;
    private String cName;
    private String cDuration;
}
