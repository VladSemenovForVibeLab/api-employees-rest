package ru.svf.Employees.Management.Project.model.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CourseModelRequest {
    private String cId;
    private String cName;
    private String cDuration;
}
