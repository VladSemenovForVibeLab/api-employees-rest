package ru.svf.Employees.Management.Project.service.interf;

import ru.svf.Employees.Management.Project.entity.SubjectModel;
import ru.svf.Employees.Management.Project.model.request.AddStudentsInSubjectRequest;
import ru.svf.Employees.Management.Project.model.request.SubjectModelRequest;
import ru.svf.Employees.Management.Project.model.response.StudentModelResponse;
import ru.svf.Employees.Management.Project.model.response.SubjectModelResponse;

import java.util.List;

public interface ISubjectModelService extends CRUDService<SubjectModelRequest, SubjectModelResponse, String, SubjectModel> {
    List<StudentModelResponse> addStudentsInSubjects(AddStudentsInSubjectRequest addStudentsInSubjectRequest);
}
