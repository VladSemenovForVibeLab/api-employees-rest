package ru.svf.Employees.Management.Project.util;

import org.springframework.stereotype.Component;
import ru.svf.Employees.Management.Project.entity.ProfessorModel;
import ru.svf.Employees.Management.Project.model.request.ProfessorModelRequest;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProfessorModelDTOUtil implements EntityDTOUtil<ProfessorModel, ProfessorModelRequest, ProfessorModelResponse> {
    @Override
    public ProfessorModelRequest toDtoModelRequest(ProfessorModel entityModel) {
        return ProfessorModelRequest
                .builder()
                .sEmail(entityModel.getSEmail())
                .sId(entityModel.getSId())
                .sName(entityModel.getSName())
                .sPhone(entityModel.getSPhone())
                .build();
    }

    @Override
    public ProfessorModelResponse toDtoModelResponseFromDtoModelRequest(ProfessorModelRequest dtoModel) {
        return null;
    }

    @Override
    public ProfessorModelResponse toDtoModelResponse(ProfessorModel entityModel) {
        return ProfessorModelResponse
                .builder()
                .sId(entityModel.getSId())
                .sEmail(entityModel.getSEmail())
                .sName(entityModel.getSName())
                .sPhone(entityModel.getSPhone())
                .build();
    }

    @Override
    public ProfessorModel toEntityModelFromRequest(ProfessorModelRequest dtoModel) {
        return ProfessorModel
                .builder()
                .sEmail(dtoModel.getSEmail())
                .sPhone(dtoModel.getSPhone())
                .sName(dtoModel.getSName())
                .sId(dtoModel.getSId())
                .build();
    }

    @Override
    public ProfessorModel toEntityModelFromResposne(ProfessorModelResponse dtoModel) {
        return ProfessorModel
                .builder()
                .sId(dtoModel.getSId())
                .sEmail(dtoModel.getSEmail())
                .sPhone(dtoModel.getSPhone())
                .sName(dtoModel.getSName())
                .build();
    }

    @Override
    public List<ProfessorModelRequest> toDtoModelsRequests(List<ProfessorModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProfessorModelResponse> toDtoModelsResponse(List<ProfessorModel> entityModels) {
        return entityModels
                .stream()
                .map(this::toDtoModelResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProfessorModel> toEntityModelsFromRequest(List<ProfessorModelRequest> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromRequest)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProfessorModel> toEntityModelsFromResponse(List<ProfessorModelResponse> dtoModels) {
        return dtoModels
                .stream()
                .map(this::toEntityModelFromResposne)
                .collect(Collectors.toList());
    }
}
