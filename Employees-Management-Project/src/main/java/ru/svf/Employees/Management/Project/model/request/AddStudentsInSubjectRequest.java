package ru.svf.Employees.Management.Project.model.request;

import lombok.*;

import java.util.List;
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class AddStudentsInSubjectRequest {
    List<String> studentIds;
    List<String> subjectIds;
}
