package ru.svf.Employees.Management.Project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.svf.Employees.Management.Project.entity.CourseModel;
import ru.svf.Employees.Management.Project.model.request.CourseModelRequest;
import ru.svf.Employees.Management.Project.model.response.CourseModelResponse;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;
import ru.svf.Employees.Management.Project.service.interf.CourseModelService;

@RestController
@RequestMapping(CourseModelRestController.COURSE_MODEL_REST_CONTROLLER)
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:5173")
public class CourseModelRestController extends CRUDRESTController<CourseModelRequest, CourseModelResponse, String, CourseModel> {
    private final CourseModelService courseModelService;
    public static final String COURSE_MODEL_REST_CONTROLLER = "/courses";

    @Override
    CRUDService<CourseModelRequest, CourseModelResponse, String, CourseModel> getService() {
        return courseModelService;
    }
}
