package ru.svf.Employees.Management.Project.model.request;

import jakarta.persistence.Column;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentModelRequest {
    private String sId;
    private String sName;
    private String sEmail;
    private String sPhone;
}
