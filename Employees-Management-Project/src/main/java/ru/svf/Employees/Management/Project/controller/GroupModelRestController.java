package ru.svf.Employees.Management.Project.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.svf.Employees.Management.Project.entity.GroupModel;
import ru.svf.Employees.Management.Project.model.request.AddProfessorInGroupRequest;
import ru.svf.Employees.Management.Project.model.request.GroupModelRequest;
import ru.svf.Employees.Management.Project.model.response.GroupModelResponse;
import ru.svf.Employees.Management.Project.model.response.ProfessorModelResponse;
import ru.svf.Employees.Management.Project.service.interf.CRUDService;
import ru.svf.Employees.Management.Project.service.interf.IGroupModelService;

import java.util.List;

@RestController
@RequestMapping(GroupModelRestController.GROUP_MODEL_REST_CONTROLLER)
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:5173")
public class GroupModelRestController extends CRUDRESTController<GroupModelRequest, GroupModelResponse, String, GroupModel> {
    private final IGroupModelService groupModelService;
    public static final String GROUP_MODEL_REST_CONTROLLER = "/groups";
    @Override
    CRUDService<GroupModelRequest, GroupModelResponse, String, GroupModel> getService() {
        return groupModelService;
    }

    @PostMapping("/add-professor")
    public ResponseEntity<List<ProfessorModelResponse>> addProfessorInGroup(
            @RequestBody AddProfessorInGroupRequest addProfessorModelRequest
    ) {
        return new ResponseEntity<>(groupModelService.addProfessorInGroup(addProfessorModelRequest), HttpStatus.OK);
    }
}
