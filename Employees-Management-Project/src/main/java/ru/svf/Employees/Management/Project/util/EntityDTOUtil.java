package ru.svf.Employees.Management.Project.util;

import java.util.List;

public interface EntityDTOUtil<E,R,P> {
    R toDtoModelRequest(E entityModel);
    P toDtoModelResponseFromDtoModelRequest(R dtoModel);
    P toDtoModelResponse(E entityModel);
    E toEntityModelFromRequest(R dtoModel);
    E toEntityModelFromResposne(P dtoModel);
    List<R> toDtoModelsRequests(List<E> entityModels);
    List<P> toDtoModelsResponse(List<E> entityModels);
    List<E> toEntityModelsFromRequest(List<R> dtoModels);
    List<E> toEntityModelsFromResponse(List<P> dtoModels);

}
