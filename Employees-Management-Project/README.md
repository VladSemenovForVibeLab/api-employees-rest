# Учет студентов на Spring Boot

Данное приложение предназначено для учета студентов и основано на фреймворке Spring Boot с использованием следующих зависимостей:

- Spring Boot 3.2.0
- Maven
- Java 17
- Springdoc 2.1.0
- Spring Data JPA
- Spring JDBC
- Spring Web
- Spring Data REST
- Spring HATEOAS
- Spring DevTools
- MariaDB
- PostgreSQL
- Lombok
- Starter Test

## Запуск приложения

Для запуска приложения необходимо выполнить следующие шаги:

1. Убедитесь, что у вас установлены Java 17, Maven и MariaDB/PostgreSQL.
2. Создайте базу данных с именем `students`.
3. Отредактируйте файл `application.properties` и укажите настройки подключения к базе данных.
4. Скомпилируйте приложение с помощью команды `mvn clean install`.
5. Запустите приложение с помощью команды `java -jar *.jar`.

## Документация API

Документация API доступна по адресу [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).
